﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageLayout.Master" AutoEventWireup="true" CodeBehind="DomainsList.aspx.cs" Inherits="Wiki.DomainsList" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MasterPageLayoutCPH" runat="server">
    <h1>Domains:</h1>
    <asp:GridView ID="GridViewDomains" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" DataSourceID="SqlDataSource1" 
        onselectedindexchanged="GridViewDomains_SelectedIndexChanged">
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="domain" HeaderText="domain" 
                SortExpression="domain" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:UsersConnectionString %>" 
        
        SelectCommand="SELECT DISTINCT [domain] FROM [Articles] WHERE ([lastVersion] = @lastVersion)">
        <SelectParameters>
            <asp:Parameter DefaultValue="True" Name="lastVersion" Type="Boolean" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
