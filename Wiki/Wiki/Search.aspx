﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageLayout.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="Wiki.Search" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MasterPageLayoutCPH" runat="server">
    <h1>Search results:</h1>
    <asp:GridView ID="GridViewSearch" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="id" DataSourceID="SqlDataSource1" AllowPaging="True" 
        AllowSorting="True" 
        onselectedindexchanged="GridViewSearch_SelectedIndexChanged">
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" 
                ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
            <asp:BoundField DataField="date" HeaderText="date" SortExpression="date" />
            <asp:BoundField DataField="domain" HeaderText="domain" 
                SortExpression="domain" />
            <asp:TemplateField HeaderText="content" SortExpression="artContent">
                <ItemTemplate>
                    <asp:Literal ID="LiteralContent" runat="server" Text='<%#Eval("artContent")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:UsersConnectionString %>" 
        SelectCommand="">
        <SelectParameters>
            <asp:Parameter DefaultValue="True" Name="lastVersion" Type="Boolean" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
