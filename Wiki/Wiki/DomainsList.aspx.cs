﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Wiki
{
    public partial class DomainsList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GridViewDomains_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["domain"] = GridViewDomains.SelectedRow.Cells[1].Text;
            Response.Redirect("DomainSpecificPage.aspx");
        }
    }
}