﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Wiki
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["error"] != null)
            {
                Response.Write(Session["error"].ToString());
                Session["error"] = null;
            }
            if (Session["type"] == null)
            {
                Session["type"] = "unregistered";
            }
        }

        protected void GridViewArticles_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["article"] = GridViewArticles.SelectedRow.Cells[1].Text;
            Response.Redirect("ViewArticle.aspx");
        }
    }
}