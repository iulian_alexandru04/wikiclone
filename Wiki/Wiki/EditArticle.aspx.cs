﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Wiki
{
    public partial class EditArticle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UsersConnectionString"].ConnectionString);
                con.Open();
                string getarticle = "select * from Articles where id=" + Session["article"].ToString();
                SqlCommand com = new SqlCommand(getarticle, con);
                SqlDataReader sqlReader = com.ExecuteReader();
                if (!sqlReader.HasRows) throw new Exception("articlol nu poate fi gasit");
                sqlReader.Read();
                if (!IsPostBack)
                {
                    TextBoxTitle.Text = sqlReader["title"].ToString();
                    TextBoxDomain.Text = sqlReader["domain"].ToString();
                    TextBoxContent.Text = sqlReader["artContent"].ToString();
                }
                if (Session["type"].ToString() == "admin" || Session["type"].ToString() == "editor")
                {
                    TextBoxTitle.Enabled = true;
                    TextBoxDomain.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Home.aspx");
                Session["error"]="Error:" + ex.ToString(); 
            }
            
        }

        protected void ButtonPreview_Click(object sender, EventArgs e)
        {
            LabelPreview.Visible = true;
            Literal1.Text = TextBoxContent.Text;
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UsersConnectionString"].ConnectionString);
                con.Open();
                string checkarticle = "select * from Articles where id=" + Session["article"].ToString();
                SqlCommand com1 = new SqlCommand(checkarticle, con);
                SqlDataReader sqlReader = com1.ExecuteReader();
                if (!sqlReader.HasRows) throw new Exception("articolul nu poate fi gasit");
                sqlReader.Read();
                string inserArticle = "insert into Articles (title,date,domain,artContent,lastVersion,crtVersion,protected,initAuthor,lastUser,initVerID) values (@Title,getdate(),@Domain,@Content,1,@Version,@Protected,@Author,@User,@InitVerID)";
                SqlCommand com2 = new SqlCommand(inserArticle, con);
                com2.Parameters.AddWithValue("@Title", TextBoxTitle.Text);
                com2.Parameters.AddWithValue("@Domain", TextBoxDomain.Text);
                com2.Parameters.AddWithValue("@Content", TextBoxContent.Text);
                int protection;
                if(sqlReader["protected"].ToString()=="True") protection=1; else protection=0;
                com2.Parameters.AddWithValue("@Protected", protection);
                com2.Parameters.AddWithValue("@Version", int.Parse(sqlReader["crtVersion"].ToString())+1);
                if(sqlReader.IsDBNull(8))
                    com2.Parameters.AddWithValue("@Author", DBNull.Value);
                else
                    com2.Parameters.AddWithValue("@Author", sqlReader["initAuthor"].ToString());
                if (Session["username"] == null)
                    com2.Parameters.AddWithValue("@User", DBNull.Value);
                else
                    com2.Parameters.AddWithValue("@User", Session["username"].ToString());
                com2.Parameters.AddWithValue("@InitVerID", Session["initVersion"].ToString());
                sqlReader.Close();
                com2.ExecuteNonQuery();
                string updateArticle = "update Articles set lastVersion=0 where id="+Session["article"].ToString();
                SqlCommand com3 = new SqlCommand(updateArticle, con);
                com3.ExecuteNonQuery();
                con.Close();
                Response.Write("Article added successfuly.");
                Response.Redirect("Home.aspx");
            }
            catch (Exception ex)
            { Response.Write("Error:" + ex.ToString()); }
        }
    }
}