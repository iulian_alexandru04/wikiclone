﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageLayout.Master" AutoEventWireup="true" CodeBehind="EditArticle.aspx.cs" Inherits="Wiki.EditArticle" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="EditArticle" ContentPlaceHolderID="MasterPageLayoutCPH" runat="server">
    <h1>Edit article page</h1>
    <table class="style1">
        <tr>
            <td class="style2">
                <asp:Label ID="LabelTitle" runat="server" Text="Title:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxTitle" runat="server" Enabled="False"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTitle" runat="server" 
                    ErrorMessage="Title is required." ControlToValidate="TextBoxTitle" 
                    ForeColor="#FF4444"></asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Label ID="LabelDomain" runat="server" Text="Domain:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxDomain" runat="server" Enabled="False"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDomain" runat="server" 
                    ControlToValidate="TextBoxDomain" ErrorMessage="Domain is required." 
                    ForeColor="#FF4444"></asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Label ID="LabelContent" runat="server" Text="Content:"></asp:Label>
            </td>
            <td>
                <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
                <asp:TextBox ID="TextBoxContent" runat="server" Rows="10" TextMode="MultiLine" Columns="80"></asp:TextBox>
                <ajaxToolkit:HtmlEditorExtender ID="HtmlEditorExtenderContent" runat="server" TargetControlID="TextBoxContent" EnableSanitization="False"></ajaxToolkit:HtmlEditorExtender>
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorContent" runat="server" 
                    ErrorMessage="Some content is required." ControlToValidate="TextBoxContent" 
                    ForeColor="#FF4444"></asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <asp:Button ID="ButtonPreview" runat="server" onclick="ButtonPreview_Click" 
                    Text="Preview" />
    <asp:Button ID="ButtonSubmit" runat="server" onclick="ButtonSubmit_Click" 
        Text="Submit" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <asp:Label ID="LabelPreview" runat="server" Text="Preview:" Visible="False"></asp:Label>
    <br />
    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
</asp:Content>
