﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageLayout.Master" AutoEventWireup="true" CodeBehind="DomainSpecificPage.aspx.cs" Inherits="Wiki.DomainSpecificPage" %>
<asp:Content ID="DomainSpecificPage" ContentPlaceHolderID="MasterPageLayoutCPH" runat="server">
    <h1><asp:Label ID="LabelDomain" runat="server" Text="Label"></asp:Label></h1>
    <asp:GridView ID="GridViewDomainArticles" runat="server" 
        AutoGenerateColumns="False" DataSourceID="SqlDataSource1" 
        AllowPaging="True" AllowSorting="True" 
        onselectedindexchanged="GridViewDomainArticles_SelectedIndexChanged" 
        DataKeyNames="id" Height="300px" Width="750px">
        <Columns>
            <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" 
                InsertVisible="False" ReadOnly="True" />
            <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
            <asp:BoundField DataField="date" HeaderText="date" 
                SortExpression="date" />
            
            <asp:BoundField DataField="domain" HeaderText="domain" 
                SortExpression="domain" />
            
            
            <asp:TemplateField HeaderText="content" SortExpression="artContent">
                <ItemTemplate>
                    <asp:Literal ID="LiteralContent" runat="server" Text='<%#Eval("artContent")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
            
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:UsersConnectionString %>" 
        
        
        SelectCommand="SELECT [id], [title], [date], [domain], [artContent] FROM [Articles] WHERE (([lastVersion] = @lastVersion) AND ([domain] = @domain)) ORDER BY [date] DESC">
        <SelectParameters>
            <asp:Parameter DefaultValue="True" Name="lastVersion" Type="Boolean" />
            <asp:SessionParameter DefaultValue="none" Name="domain" SessionField="domain" 
                Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
