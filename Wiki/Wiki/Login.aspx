﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Wiki.Login" MasterPageFile="~/MasterPageLayout.Master" %>

<asp:Content ID="Login" ContentPlaceHolderID=MasterPageLayoutCPH runat=server>
    <h1>Login Page</h1>
    <table class="style2">
        <tr>
            <td class="style3">
                UserName:</td>
            <td class="style5">
                <asp:TextBox ID="TextBoxUserName" runat="server" Width="150px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="TextBoxUserName" ErrorMessage="UserName is required." 
                    ForeColor="#FF4444"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style3">
                Password:</td>
            <td class="style5">
                <asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password" 
                    Width="150px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="TextBoxPassword" ErrorMessage="Password is required." 
                    ForeColor="#FF4444"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td class="style5">
                <asp:Button ID="ButtonLogin" runat="server" onclick="ButtonLogin_Click" 
                    Text="Login" Width="50px" />
            </td>
            <td>
                <asp:HyperLink ID="HyperLinkRegister" runat="server" 
                    NavigateUrl="~/Registration.aspx">Register Here</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td class="style5">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>