﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Wiki
{
    public partial class DomainSpecificPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["domain"] != null)
            {
                LabelDomain.Text = Session["domain"].ToString();
            }
            else
            {
                Response.Redirect("Home.aspx");
            }
        }

        protected void GridViewDomainArticles_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["article"] = GridViewDomainArticles.SelectedRow.Cells[1].Text;
            Response.Redirect("ViewArticle.aspx");
        }
    }
}