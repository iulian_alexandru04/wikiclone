﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Wiki
{
    public partial class Search : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string[] separators = {",", ".", "!", "?", ";", ":", " "};
            string[] words = Session["search"].ToString().Split(separators, StringSplitOptions.RemoveEmptyEntries);
            string cond = "";
            foreach (var word in words)
                cond += " artContent LIKE '%" + word + "%' or";
            cond = cond.Remove(cond.Length - 2);
            SqlDataSource1.SelectCommand = "SELECT [id], [title], [date], [domain], [artContent] FROM [Articles] WHERE ([lastVersion] = @lastVersion and ("+cond+")) ORDER BY [date] DESC";
        }

        protected void GridViewSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["article"] = GridViewSearch.SelectedRow.Cells[1].Text;
            Response.Redirect("ViewArticle.aspx");
        }
    }
}