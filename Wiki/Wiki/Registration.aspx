﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="Wiki.Registration" MasterPageFile="~/MasterPageLayout.Master"%>

<asp:Content ID="Registration" ContentPlaceHolderID=MasterPageLayoutCPH runat=server>
    <h1>Registration Page</h1>
    <br />      
    
        <table class="style1">
            <tr>
                <td class="style2">
                    User Name:</td>
                <td class="style4">
                    <asp:TextBox ID="TextBoxUN" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td class="style5">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="TextBoxUN" ErrorMessage="User Name is required." 
                        ForeColor="#FF4444"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    E-mail:</td>
                <td class="style4">
                    <asp:TextBox ID="TextBoxEmail" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td class="style5">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="TextBoxEmail" ErrorMessage="E-mail is required." 
                        ForeColor="#FF4444"></asp:RequiredFieldValidator>
                    <br />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ControlToValidate="TextBoxEmail" ErrorMessage="Invalid e-mail." 
                        ForeColor="#FF4444" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Password:</td>
                <td class="style4">
                    <asp:TextBox ID="TextBoxPass" runat="server" TextMode="Password" Width="150px"></asp:TextBox>
                </td>
                <td class="style5">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="TextBoxPass" ErrorMessage="Password is required." 
                        ForeColor="#FF4444"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Confirm Password:</td>
                <td class="style4">
                    <asp:TextBox ID="TextBoxRPass" runat="server" TextMode="Password" Width="150px"></asp:TextBox>
                </td>
                <td class="style5">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="TextBoxRPass" ErrorMessage="Confirm Password is required." 
                        ForeColor="#FF4444"></asp:RequiredFieldValidator>
                    <br />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToCompare="TextBoxPass" ControlToValidate="TextBoxRPass" 
                        ErrorMessage="Password doesn't match." ForeColor="#FF4444"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Country:</td>
                <td class="style4">
                    <asp:DropDownList ID="DropDownListCountry" runat="server" Width="150px">
                        <asp:ListItem>Select Country</asp:ListItem>
                        <asp:ListItem>USA</asp:ListItem>
                        <asp:ListItem>Canada</asp:ListItem>
                        <asp:ListItem>Romania</asp:ListItem>
                        <asp:ListItem>Germany</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="style5">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ControlToValidate="DropDownListCountry" ErrorMessage="Country required." 
                        ForeColor="#FF4444" InitialValue="Select Country"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td class="style4">
                    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Submit" />
                    <input id="Reset1" type="reset" value="reset" /></td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td class="style4">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>