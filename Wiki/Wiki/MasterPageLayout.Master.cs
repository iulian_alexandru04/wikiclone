﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Wiki
{
    public partial class MasterPageLayout : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] != null)
            {
                LabelWelcome.Text = "Welcome " + Session["username"].ToString();
                ButtonLogout.Visible = true;
                HyperLinkLogin.Visible = false;
            }
            else
            {
                LabelWelcome.Visible = false;
                ButtonLogout.Visible = false;
                HyperLinkLogin.Visible = true;
            }
            if (Session["type"]!=null && Session["type"].ToString() == "admin")
            {
                HyperLinkUsers.Visible = true;
            }
        }

        protected void ButtonLogout_Click(object sender, EventArgs e)
        {
            Session["username"] = null;
            Session["type"] = null;
            Response.Redirect("Home.aspx");
        }

        protected void ImageButtonSearch_Click(object sender, ImageClickEventArgs e)
        {
            Session["search"] = TextBoxSearch.Text;
            Response.Redirect("Search.aspx");
        }
    }
}