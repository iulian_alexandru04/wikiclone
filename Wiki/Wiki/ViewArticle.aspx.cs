﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Wiki
{
    public partial class ViewArticle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["article"] != null)
            {
                try
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UsersConnectionString"].ConnectionString);
                    con.Open();
                    string getArticle = "select * from Articles where id=" + Session["article"].ToString();
                    SqlCommand com = new SqlCommand(getArticle, con);
                    SqlDataReader sqlReader = com.ExecuteReader();
                    sqlReader.Read();
                    LabelTitle.Text = sqlReader["title"].ToString();
                    LabelDomain.Text = "(" + sqlReader["domain"].ToString() + ")";
                    LiteralContent.Text = sqlReader["artContent"].ToString();
                    Session["initVersion"] = sqlReader["initVerID"];
                    if (sqlReader["protected"].ToString() == "True" && Session["type"].ToString() == "unregistered")
                    {
                        LinkButtonEdit.Visible = false;
                    }
                    if ((Session["type"].ToString() == "admin" || Session["type"].ToString() == "editor") && sqlReader["initVerID"].ToString() != Session["article"].ToString())
                    {
                        LinkButtonPrevious.Visible = true;
                    }
                    if (Session["type"].ToString() == "admin" || Session["type"].ToString() == "editor" || (Session["username"]!=null && Session["username"].ToString() == sqlReader["initAuthor"].ToString()))
                    {
                        ButtonProtect.Visible = true;
                    }
                    if (sqlReader["protected"].ToString() == "False")
                    {
                        ButtonProtect.Text = "Protect";
                    }
                    else
                    {
                        ButtonProtect.Text = "Unprotect";
                    }
                    con.Close();
                }
                catch (Exception ex)
                {
                    Session["error"] = "Error:" + ex.ToString();
                    Response.Redirect("Home.aspx");
                }

            }
            else
            {
                Response.Redirect("Home.aspx");
            }
        }

        protected void LinkButtonEdit_Click(object sender, EventArgs e)
        {
            Response.Redirect("EditArticle.aspx");
        }

        protected void ButtonProtect_Click(object sender, EventArgs e)
        {
            string newProtection;
            if (ButtonProtect.Text == "Protect")
                newProtection = "1"; //true
            else
                newProtection = "0"; //false
            string changeProtection = "update Articles set protected=" + newProtection + " where id=" + Session["article"].ToString();
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UsersConnectionString"].ConnectionString);
                con.Open();
                SqlCommand com = new SqlCommand(changeProtection, con);
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                Session["error"] = "Error:" + ex.ToString();
                Response.Redirect("Home.aspx");
            }
            Response.Redirect("ViewArticle.aspx");
        }

        protected void LinkButtonPrevious_Click(object sender, EventArgs e)
        {
            Response.Redirect("PreviousVerions.aspx");
        }

        protected void LinkButtonDelete_Click(object sender, EventArgs e)
        {
            string deleteArticle = "delete from Articles where initVerID=" + Session["initVersion"].ToString();
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UsersConnectionString"].ConnectionString);
                con.Open();
                SqlCommand com = new SqlCommand(deleteArticle, con);
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                Session["error"] = "Error:" + ex.ToString();
                Response.Redirect("Home.aspx");
            }
            Response.Redirect("Home.aspx");
        }
    }
}