﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Wiki.Home" MasterPageFile="~/MasterPageLayout.Master"%>

<asp:Content ID="home" ContentPlaceHolderID="MasterPageLayoutCPH" runat="server">
    <h1>Home page</h1>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:UsersConnectionString %>" 
        
        
        SelectCommand="SELECT [id], [title], [date], [domain], [artContent] FROM [Articles] WHERE ([lastVersion] = 1) ORDER BY [date] DESC"></asp:SqlDataSource>
    <asp:GridView ID="GridViewArticles" runat="server" DataSourceID="SqlDataSource1" 
        AllowPaging="True" AutoGenerateColumns="False" 
        onselectedindexchanged="GridViewArticles_SelectedIndexChanged">
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" />
            <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
            <asp:BoundField DataField="date" HeaderText="date" 
                SortExpression="date" />
            
            <asp:BoundField DataField="domain" HeaderText="domain" 
                SortExpression="domain" />
            <asp:TemplateField HeaderText="content" SortExpression="artContent">
                <ItemTemplate>
                    <asp:Literal ID="LiteralContent" runat="server" Text='<%#Eval("artContent")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
    </asp:GridView>
</asp:Content>