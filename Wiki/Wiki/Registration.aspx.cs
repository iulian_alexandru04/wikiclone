﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Wiki
{
    public partial class Registration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UsersConnectionString"].ConnectionString);
                con.Open();
                string checkuser = "select count(*) from Users where username='" + TextBoxUN.Text + "'";
                SqlCommand com1 = new SqlCommand(checkuser, con);
                int temp = Convert.ToInt32(com1.ExecuteScalar().ToString());
                if (temp == 0)
                {
                    string insertuser = "insert into Users (username,email,password,country,type) values (@Uname,@email,@password,@country,@Type)";
                    SqlCommand com2 = new SqlCommand(insertuser, con);
                    com2.Parameters.AddWithValue("@Uname", TextBoxUN.Text);
                    com2.Parameters.AddWithValue("@email", TextBoxEmail.Text);
                    com2.Parameters.AddWithValue("@password", TextBoxPass.Text);
                    com2.Parameters.AddWithValue("@country", DropDownListCountry.Text);
                    com2.Parameters.AddWithValue("@Type", "registered");
                    com2.ExecuteNonQuery();
                    Response.Write("Registration is successful");
                    Response.Redirect("Home.aspx");
                }
                else
                {
                    Response.Write("User already Exists");
                }
                con.Close();
            }
            catch (Exception ex)
            { Response.Write("Error:"+ex.ToString()); }
        }
    }
}