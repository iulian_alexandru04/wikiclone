﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageLayout.Master" AutoEventWireup="true" CodeBehind="PreviousVerions.aspx.cs" Inherits="Wiki.PreviousVerions" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MasterPageLayoutCPH" runat="server">
    <h1>Previous Verions:</h1>
    <asp:Label ID="LabelSetVersion" runat="server" Text="Set to version:"></asp:Label>
    <asp:TextBox ID="TextBoxSet" runat="server"></asp:TextBox>
    <asp:Button ID="ButtonSet" runat="server" Text="Set" 
        onclick="ButtonSet_Click" />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
        AllowSorting="True" DataSourceID="SqlDataSource1" AutoGenerateColumns="False">
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" />
            <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
            <asp:BoundField DataField="date" HeaderText="date" SortExpression="date" />
            <asp:BoundField DataField="crtVersion" HeaderText="version" SortExpression="crtVersion" />
            <asp:BoundField DataField="domain" HeaderText="domain" SortExpression="domain" />
            <asp:BoundField DataField="lastUser" HeaderText="last user to edit" SortExpression="lastUser" />
            <asp:TemplateField HeaderText="content" SortExpression="artContent">
                <ItemTemplate>
                    <asp:Literal ID="LiteralContent" runat="server" Text='<%#Eval("artContent")%>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:UsersConnectionString %>" 
        SelectCommand="SELECT [id], [title], [date], [domain], [artContent], [crtVersion], [lastUser] FROM [Articles] WHERE ([initVerID] = @initVerID) ORDER BY [crtVersion] DESC">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="none" Name="initVerID" 
                SessionField="initVersion" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
