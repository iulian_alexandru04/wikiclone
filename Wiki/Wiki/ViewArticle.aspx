﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageLayout.Master" AutoEventWireup="true" CodeBehind="ViewArticle.aspx.cs" Inherits="Wiki.ViewArticle" %>
<asp:Content ID="ViewArticle" ContentPlaceHolderID="MasterPageLayoutCPH" runat="server">
    <h1>
    <asp:Label ID="LabelTitle" runat="server" Text="Title" Visible="True"></asp:Label>
    </h1>
    <h3><asp:Label ID="LabelDomain" runat="server" Text="Label"></asp:Label></h3>
    <hr/>
    <asp:Literal ID="LiteralContent" runat="server"></asp:Literal>

<br />
<asp:LinkButton ID="LinkButtonEdit" runat="server" onclick="LinkButtonEdit_Click">Edit</asp:LinkButton>

    <asp:LinkButton ID="LinkButtonDelete" runat="server" 
        onclick="LinkButtonDelete_Click">Delete</asp:LinkButton>
    <asp:LinkButton ID="LinkButtonPrevious" runat="server" 
        onclick="LinkButtonPrevious_Click" Visible="False">Previous Versions</asp:LinkButton>

    <asp:Button ID="ButtonProtect" runat="server" onclick="ButtonProtect_Click" 
        Text="Protect" Visible="False" />

</asp:Content>
