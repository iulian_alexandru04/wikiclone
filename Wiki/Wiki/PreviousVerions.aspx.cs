﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Wiki
{
    public partial class PreviousVerions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSet_Click(object sender, EventArgs e)
        {
            try 
            {
                string setLastVersion = "update Articles set lastVersion=1 where (initVerID="+Session["initVersion"]+" and crtVersion="+TextBoxSet.Text+")";
                string deleteNewerVersions = "delete from Articles where (initVerID=" + Session["initVersion"] + " and crtVersion>" + TextBoxSet.Text + ")";
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UsersConnectionString"].ConnectionString);
                con.Open();
                SqlCommand com1 = new SqlCommand(setLastVersion, con);
                com1.ExecuteNonQuery();
                SqlCommand com2 = new SqlCommand(deleteNewerVersions, con);
                com2.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                Session["error"] = "Error:" + ex.ToString();
                Response.Redirect("Home.aspx");
            }
            Response.Redirect("Home.aspx");
        }
    }
}