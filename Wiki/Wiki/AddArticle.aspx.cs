﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Wiki
{
    public partial class AddArticle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["UsersConnectionString"].ConnectionString);
                con.Open();
                string checkarticle = "select count(*) from Articles where title='" + TextBoxTitle.Text + "'";
                SqlCommand com1 = new SqlCommand(checkarticle, con);
                int temp = Convert.ToInt32(com1.ExecuteScalar().ToString());
                if (temp == 0)
                {
                    string inserArticle = "insert into Articles (title,date,domain,artContent,lastVersion,crtVersion,protected,initAuthor,lastUser) values (@Title,getdate(),@Domain,@Content,1,1,0,@Author,@Author)";
                    SqlCommand com2 = new SqlCommand(inserArticle, con);
                    com2.Parameters.AddWithValue("@Title", TextBoxTitle.Text);
                    com2.Parameters.AddWithValue("@Domain", TextBoxDomain.Text);
                    com2.Parameters.AddWithValue("@Content", TextBoxContent.Text);
                    if (Session["username"] == null)
                        com2.Parameters.AddWithValue("@Author", DBNull.Value);
                    else
                        com2.Parameters.AddWithValue("@Author",Session["username"].ToString());
                    
                    com2.ExecuteNonQuery();
                    string setInitialVersion = "update Articles set initVerID = id where initVerID is null";
                    SqlCommand com3 = new SqlCommand(setInitialVersion, con);
                    com3.ExecuteNonQuery();
                    con.Close();
                    Response.Write("Article added successfuly.");
                    Response.Redirect("Home.aspx");
                }
                else
                {
                    con.Close();
                    Response.Write("Article with same title already exists.");
                }
            }
            catch (Exception ex)
            { Response.Write("Error:" + ex.ToString()); }
        }

        protected void ButtonPreview_Click(object sender, EventArgs e)
        {
            LabelPreview.Visible = true;
            Literal1.Text = TextBoxContent.Text;
        }
    }
}